import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { WpService } from '../services/wp.service';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.css']
})
export class PageComponent implements OnInit {

  busy: boolean;
  user: any;
  page: any;
  error: boolean;
  page_id: any;

  constructor(private title: Title, private route: ActivatedRoute, private wp: WpService) { }

  ngOnInit() {
    const slug = this.route.snapshot.params['page_slug'];
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.page_id = params.get('page_id');
      this.loadPage(this.page_id);
    });
  }

  loadPage(page_id) {
    this.busy = true;
    this.wp.get_page(page_id)
      .subscribe((res) => {
        this.page = res;
        this.busy = false;
        this.title.setTitle(res['title'].rendered);
      }, (error) => {
        this.error = true;
      });
  }


  doRefresh(event) {
    console.log(event);
    this.loadPage(this.page_id);
    this.error = false;
  }

}
