import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { PostDetailComponent } from './post-detail/post-detail.component';
import { CategoryComponent } from './category/category.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { AuthorComponent } from './author/author.component';
import { CategoryListComponent } from './category-list/category-list.component';
import { PageComponent } from './page/page.component';
import { TagListComponent } from './tag-list/tag-list.component';
import { TagComponent } from './tag/tag.component';

const routes: Routes = [
  { path: '', component: HomeComponent, data: { state: 'home' } },
  { path: 'category/all', component: CategoryListComponent, data: { state: 'all' } },
  { path: 'tag/all', component: TagListComponent, data: { state: 'tags'} },
  { path: 'author/:author_id', component: AuthorComponent, data: { state: 'author' } },
  { path: 'page/:slug/:page_id', component: PageComponent, data: { state: 'page' } },
  { path: 'tag/:tag_id/:tag_slug', component: TagComponent, data: { state: 'tag' } },
  { path: 'category/:category_id/:category_slug', component: CategoryComponent, data: { state: 'category' } },
  { path: ':slug/:post_id', component: PostDetailComponent, data: { state: 'post' } },
  { path: '**', component: NotfoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
