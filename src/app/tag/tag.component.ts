import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { WpService } from '../services/wp.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-tag',
  templateUrl: './tag.component.html',
  styleUrls: ['./tag.component.css']
})
export class TagComponent implements OnInit {

  busy_cat: boolean;
  busy: boolean;
  tag: any;
  posts: any;
  end: boolean;

  infinite_busy: boolean;

  throttle = 30;
  scrollDistance = 1;
  scrollUpDistance = 2;
  direction = '';
  page_count = 1;
  error: boolean;
  tag_id: any;


  constructor(private router: Router,
    private wp: WpService,
    private route: ActivatedRoute,
    private title: Title) { }

  ngOnInit() {
    this.tag_id = this.route.snapshot.params['tag_id'];
    const tag_slug = this.route.snapshot.params['tag_slug'];

    this.loadTag(this.tag_id);
    this.loadTagPosts(this.tag_id);
    this.title.setTitle('Category');
  }

  loadTag(tag_id) {
    this.busy_cat = true;
    this.wp.get_tag(tag_id)
      .subscribe((res) => {
        this.busy_cat = false;
        this.tag = res;
        this.title.setTitle(res['name']);
      }, (error) => {
        this.error = true;
      });
  }

  loadTagPosts(tag_id) {
    this.busy = true;
    this.wp.get_tag_posts(tag_id)
      .subscribe((res) => {
        console.log(res);
        this.busy = false;
        this.posts = res;
      }, (error) => {
        this.error = true;
      });
  }

  doRefresh(event) {
    console.log(event);
    this.loadTag(this.tag_id);
    this.loadTagPosts(this.tag_id);
    this.error = false;
  }


}
