import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppSettings } from '../utils/constants';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';

@Injectable()
export class WpService {

  constructor(private http: HttpClient) { }

  /**
   * Get User
   * @param id id User ID
   */
  get_user(id: number) {
    return this.http.get(`${AppSettings.API_ENDPOINT}/users/${id}`)
      .map((res) => {
        return res;
      });
  }

  /**
 * Get all posts. Use 'page' for pagination
 * @param per_page The amount of posts per scoop
 * @param page For pagination purposes, the number of page
 */
  get_user_posts(author: number, per_page?: number, page?: number) {
    per_page = per_page || 10;
    page = page || 1;
    return this.http.get(`${AppSettings.API_ENDPOINT}/posts?context=embed&per_page=${per_page}&_embed&page=${page}&author=${author}`)
      .map((res) => {
        return res;
      });
  }

  /**
   * Get all posts. Use 'page' for pagination
   * @param per_page The amount of posts per scoop
   * @param page For pagination purposes, the number of page
   */
  get_posts(per_page: number, page?: number) {
    page = page || 1;
    return this.http.get(`${AppSettings.API_ENDPOINT}/posts?context=embed&per_page=${per_page}&_embed&page=${page}`)
      .map((res) => {
        return res;
      });
  }

  /**
   * Get all categories
   */
  get_categories() {
    return this.http.get(`${AppSettings.API_ENDPOINT}/categories?per_page=50`)
      .map((res) => {
        return res;
      });
  }

  /**
   * Retrieve Category
   * @param id Category ID
   */
  get_category(id: number) {
    return this.http.get(`${AppSettings.API_ENDPOINT}/categories/${id}`)
      .map((res) => {
        return res;
      });
  }

  /**
   * Get all posts matching a category
   * @param id Category ID
   * @param page For pagination purposes, the number of page
   * @param per_page How many items to retrieve in a go (useful for article sidebar category loading)
   */
  get_category_posts(id: number, page?: number, per_page?: number) {
    per_page = per_page || 100;
    page = page || 1;
    return this.http.get(`${AppSettings.API_ENDPOINT}/posts?per_page=${per_page}&context=embed&categories=${id}&_embed&page=${page}`)
      .map((res) => {
        return res;
      });
  }

  /**
   * Get Single Post/Article
   * @param id Post or Article ID
   */
  get_post(id: string) {
    return this.http.get(`${AppSettings.API_ENDPOINT}/posts/${id}?_embed`)
      .map((res) => {
        return res;
      });
  }

  /**
   * Get all tags
   */
  get_tags(page_number?: number) {
    const per_page = 100;
    page_number = page_number || 1;
    return this.http.get(`${AppSettings.API_ENDPOINT}/tags?per_page=${per_page}&page=${page_number}`)
      .map((res) => {
        return res;
      });
  }

  /**
   * Get Tag by ID
   * @param id tag ID
   */
  get_tag(id: string) {
    return this.http.get(`${AppSettings.API_ENDPOINT}/tags/${id}?_embed`)
      .map((res) => {
        return res;
      });
  }

  /**
   * Get posts matching a Tag ID
   * @param id Tag ID
   * @param page For pagination purposes, the number of page
   * @param per_page How many items to retrieve in a go (useful for article sidebar tag loading)
   */
  get_tag_posts(id: number, page?: number, per_page?: number) {
    per_page = per_page || 100;
    page = page || 1;
    return this.http.get(`${AppSettings.API_ENDPOINT}/posts/?per_page=${per_page}&context=embed&tags=${id}&page=${page}&_embed`)
      .map((res) => {
        return res;
      });
  }

  /**
   * Search posts
   * @param search keyword to use in searching
   */
  post_search(search: string) {
    return this.http.get(`${AppSettings.API_ENDPOINT}/posts?search=${search}&per_page=100`)
      .map((res) => {
        return res;
      });
  }

  /**
 * Get all pages. Use 'page' for pagination
 * @param per_page The amount of pages per scoop
 * @param page For pagination purposes, the number of page
 */
  get_pages(per_page?: number, page?: number) {
    per_page = per_page || 10;
    page = page || 1;
    return this.http.get(`${AppSettings.API_ENDPOINT}/pages?context=embed&per_page=${per_page}&_embed&page=${page}`)
      .map((res) => {
        return res;
      });
  }

  /**
 * Get Single Page
 * @param id Page ID
 */
  get_page(id: string) {
    return this.http.get(`${AppSettings.API_ENDPOINT}/pages/${id}`)
      .map((res) => {
        return res;
      });
  }
}
