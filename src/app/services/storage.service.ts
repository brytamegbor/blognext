import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppSettings } from '../utils/constants';

@Injectable()
export class StorageService {

  constructor() { }

  /**
   * An array list of posts to save, stringified.
   * @param items Array of posts
   */
  save_posts(items: any) {
    const posts = localStorage.setItem('posts', JSON.stringify(items));
    return posts;
  }

  /**
   * Remove all posts, then replace with new
   * An array list of posts to save, stringified.
   * @param new_items Array of posts
   */
  refresh_posts(new_items: any) {
    localStorage.removeItem('posts');
    const posts = localStorage.setItem('posts', JSON.stringify(new_items));
    return posts;
  }

  /**
   * Return all saved posts, JSON parsed
   */
  get_posts() {
    return JSON.parse(localStorage.getItem('posts'));
  }

  /**
   * Save the post object with the post ID as key
   * @param id ID of post to save
   * @param item The post object to save
   */
  save_post(id: string, item: any) {
    const post = localStorage.setItem(`${id}`, JSON.stringify(item));
    return post;
  }

  /**
   * Retrieve post based on ID
   * @param id Post ID to retrieve
   */
  get_post(id: string) {
    return JSON.parse(localStorage.getItem(`${id}`));
  }

  /**
   * Put new content into
   * @param id Post ID to refresh
   */
  refresh_post(id: string, items: any) {
    localStorage.removeItem(`${id}`);
    const post = localStorage.setItem(`${id}`, JSON.stringify(items));
    return post;
  }

  is_post_local(id: string): boolean {
    if (localStorage.getItem(`${id}`)) {
      return true;
    } else {
      return false;
    }
  }
}
