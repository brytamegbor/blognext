import { Component, OnInit, Input } from '@angular/core';

declare let navigator: any;

@Component({
  selector: 'app-share',
  templateUrl: './share.component.html',
  styleUrls: ['./share.component.css']
})
export class ShareComponent implements OnInit {

  @Input() post: any;

  constructor() { }

  ngOnInit() {
  }

  stripHTML(html) {
    const doc = new DOMParser().parseFromString(html, 'text/html');
    return doc.body.textContent || '';
  }

  doShare() {
    console.log('Do shared');
    console.log(navigator.share);
    if (navigator.share) {
      navigator.share({
        title: this.post.title.rendered,
        text: this.stripHTML(this.post.excerpt.rendered),
        url: `https://khophi.blog/${this.post.slug}/${this.post.id}`,
      })
        .then(() => {
          console.log('Successful share');

        })
        .catch((error) => console.log('Error sharing', error));
    }
  }

}
