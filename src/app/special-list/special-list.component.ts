import { Component, OnInit, Input } from '@angular/core';
import { WpService } from '../services/wp.service';

@Component({
  selector: 'app-special-list',
  templateUrl: './special-list.component.html',
  styleUrls: ['./special-list.component.css']
})
export class SpecialListComponent implements OnInit {

  @Input() items;
  @Input() type;

  busy: boolean;
  infinite_busy: boolean;

  throttle = 100;
  scrollDistance = 1;
  scrollUpDistance = 2;
  direction = '';
  page_count = 1;
  end: boolean;

  constructor(private wp: WpService) { }

  ngOnInit() {
    console.log(this.items);
  }

  onDown() {

    this.infinite_busy = true;
    this.direction = 'down';
    this.page_count = this.page_count + 1;

    // TODO Heavily refactor this part into single
    switch (this.type) {
      case 'category':
        this.loadCategory(this.page_count);
        break;
      case 'tag':
        this.loadTag(this.page_count);
        break;
      default:
        this.loadCategory(this.page_count);
    }
  }

  loadCategory(page_count) {
    this.wp.get_categories()
      .subscribe((res) => {
        this.infinite_busy = false;
        if (!res['length']) {
          this.end = true;
          console.log('ended');
        } else {
          console.log('continuing');
          this.items = this.items.concat(res);
        }
      });
  }

  loadTag(page_count) {
    this.wp.get_tags(page_count)
      .subscribe((res) => {
        this.infinite_busy = false;
        if (!res['length']) {
          this.end = true;
          console.log('ended');
        } else {
          console.log('continuing');
          this.items = this.items.concat(res);
        }
      });
  }

  onUp() {
    console.log('going up');
  }
}
