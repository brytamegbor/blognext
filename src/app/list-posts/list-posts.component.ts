import { Component, OnInit, Input } from '@angular/core';
import { WpService } from '../services/wp.service';
import { ngListTransition } from '../utils/transition';
import { StorageService } from '../services/storage.service';

@Component({
  selector: 'app-list-posts',
  templateUrl: './list-posts.component.html',
  styleUrls: ['./list-posts.component.css'],
  animations: [ngListTransition]

})
export class ListPostsComponent implements OnInit {

  @Input() posts: any;
  @Input() type: string;
  @Input() author_id: number;
  @Input() category_id: number;
  @Input() tag_id: number;

  busy: boolean;
  infinite_busy: boolean;

  throttle = 100;
  scrollDistance = 1;
  scrollUpDistance = 2;
  direction = '';
  page_count = 1;
  end: boolean;

  constructor(private wp: WpService, private sts: StorageService) { }

  ngOnInit() {
    // console.log(this.posts);
    // console.log(this.type);
  }

  onDown() {
    this.infinite_busy = true;
    this.direction = 'down';
    this.page_count = this.page_count + 1;

    // TODO Heavily refactor this part into single
    switch (this.type) {
      case 'all':
        this.loadPosts(this.page_count);
        break;
      case 'author':
        this.loadAuthor(this.page_count);
        break;
      case 'category':
        this.loadCategory(this.page_count);
        break;
      case 'tag':
        this.loadTag(this.page_count);
        break;
      default:
        this.loadPosts(this.page_count);
    }
  }

  // TODO Heavily refactor this part into single link
  loadCategory(page_count) {
    this.wp.get_category_posts(this.category_id, page_count)
      .subscribe((res) => {
        this.infinite_busy = false;
        if (!res['length']) {
          this.end = true;
          console.log('ended');
        } else {
          console.log('continuing');
          this.posts = this.posts.concat(res);
        }
      }, (error) => {
        if (error['error'].code === 'rest_post_invalid_page_number') {
          this.end = true;
        }
      });
  }

  loadTag(page_count) {
    console.log(page_count);
    if (!this.end) {
      this.wp.get_tag_posts(this.tag_id, page_count)
        .subscribe((res) => {
          this.infinite_busy = false;
          if (!res['length']) {
            this.end = true;
            console.log('ended');
          } else {
            console.log('continuing');
            this.posts = this.posts.concat(res);
          }
        }, (error) => {
          if (error['error'].code === 'rest_post_invalid_page_number') {
            this.end = true;
          }
        });
    }
  }

  loadAuthor(page_count) {
    this.wp.get_user_posts(this.author_id, 20, this.page_count)
      .subscribe((res) => {
        this.infinite_busy = false;
        if (!res['length']) {
          this.end = true;
          console.log('ended');
        } else {
          console.log('continuing');
          this.posts = this.posts.concat(res);
        }
      }, (error) => {
        if (error['error'].code === 'rest_post_invalid_page_number') {
          this.end = true;
        }
      });
  }

  loadPosts(page_count) {
    this.wp.get_posts(20, page_count)
      .subscribe((res) => {
        this.infinite_busy = false;
        if (!res['length']) {
          this.end = true;
          console.log('ended');
        } else {
          console.log('continuing');
          this.posts = this.posts.concat(res);
          this.sts.refresh_posts(this.posts);
        }
      }, (error) => {
        if (error['error'].code === 'rest_post_invalid_page_number') {
          this.end = true;
        }
      });
  }

  onUp() {
    this.direction = 'up';
  }

  is_saved(id) {
    return this.sts.is_post_local(id);
  }
}
