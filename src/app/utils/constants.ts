// Constants File
const DOMAIN_NAME = localStorage.getItem('use') || 'blog.khophi.co'; // enter your wordpress running site's domain

export class AppSettings {
  public static API_ENDPOINT = `https://${DOMAIN_NAME}/wp-json/wp/v2`;
}
