import { Component, OnInit } from '@angular/core';
import { WpService } from '../services/wp.service';

@Component({
  selector: 'app-tag-list',
  templateUrl: './tag-list.component.html',
  styleUrls: ['./tag-list.component.css']
})
export class TagListComponent implements OnInit {

  busy: boolean;
  tags: any;
  error: boolean;


  constructor(private wp: WpService) { }

  ngOnInit() {
    this.loadTags();
  }

  loadTags(page_count?) {
    this.busy = true;
    this.wp.get_tags()
      .subscribe((res) => {
        this.tags = res;
        this.busy = false;
      }, (error) => {
        this.error = true;
      });
  }

  doRefresh(event) {
    this.loadTags();
    this.error = false;
  }


}
