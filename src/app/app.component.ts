import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { Location, PopStateEvent } from '@angular/common';
import { WpService } from './services/wp.service';
import { routerTransition } from './utils/transition';

declare let UIkit: any;
declare let $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [routerTransition]
})
export class AppComponent implements OnInit, AfterViewInit {

  search: string;
  search_busy: boolean;
  result: any;
  pages: any;
  url: string;
  clear_try: boolean;
  site_name: string;

  private lastPoppedUrl: string;
  private yScrollStack: number[] = [];
  constructor(private router: Router,
    private wp: WpService,
    private location: Location) { }

  title = 'app';
  closeSidebar() {
    UIkit.offcanvas('#offcanvas-usage').hide();
  }

  ngAfterViewInit() {
    this.location.subscribe((ev: PopStateEvent) => {
      this.lastPoppedUrl = ev.url;
    });
    this.router.events.subscribe((ev: any) => {
      if (ev instanceof NavigationStart) {
        if (ev.url !== this.lastPoppedUrl) {
          this.yScrollStack.push(window.scrollY);
        }
      } else if (ev instanceof NavigationEnd) {
        if (ev.url === this.lastPoppedUrl) {
          this.lastPoppedUrl = undefined;
          window.scrollTo(0, this.yScrollStack.pop());
        } else {
          window.scrollTo(0, 0);
        }
      }
    });
  }

  ngOnInit() {
    let prevScrollpos = window.pageYOffset;
    window.onscroll = function () {
      const currentScrollPos = window.pageYOffset;
      if (prevScrollpos > currentScrollPos) {
        // $('#my-menu').removeClass('uk-hidden');
        document.getElementById('my-menu').style.top = '2.2vh';
        document.getElementById('up-but').style.display = 'none';
      } else {
        // $('#my-menu').addClass('uk-hidden', 2000);
        document.getElementById('up-but').style.display = 'block';
        document.getElementById('my-menu').style.top = '-65px';
      }
      prevScrollpos = currentScrollPos;
    };

    this.loadPages();
    this.site_name = localStorage.getItem('use');
    if (this.site_name) {
      this.clear_try = true;
    }

  }

  loadPages() {
    this.wp.get_pages()
      .subscribe((res) => {
        // console.log(res);
        this.pages = res;
      });
  }

  onSubmit(formData) {
    console.log(formData);
    this.search_busy = true;
    this.wp.post_search(formData.search)
      .subscribe((res) => {
        // console.log(res);
        this.result = res;
        this.search_busy = false;
      });
  }

  closeModal() {
    console.log('Close modal');
    UIkit.modal('#modal-center').hide();
  }

  getState(outlet) {
    return outlet.activatedRouteData.state;
  }

  changeURL(formData) {
    // TODO Do some conformity checks here
    if (formData.url) {
      localStorage.setItem('use', formData.url);
      window.location.reload();
      return true;
    } else {
      UIkit.notification({
        'message': 'Come on, just read the instructions.',
        'status': 'warning'
      });
    }
    if (formData === 'end') {
      localStorage.clear();
      window.location.reload();
    }
  }
}
