import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { WpService } from '../services/wp.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-author',
  templateUrl: './author.component.html',
  styleUrls: ['./author.component.css']
})
export class AuthorComponent implements OnInit {
  busy_cat: boolean;
  busy: boolean;
  user: any;
  posts: any;
  error: boolean;

  infinite_busy: boolean;

  throttle = 30;
  scrollDistance = 1;
  scrollUpDistance = 2;
  direction = '';
  page_count = 1;
  author_id: any;

  constructor(private router: Router,
    private wp: WpService,
    private route: ActivatedRoute,
    private title: Title) { }

  ngOnInit() {
    this.author_id = this.route.snapshot.params['author_id'];

    this.loadAuthor(this.author_id);
    this.loadAuthorPosts(this.author_id, 1);

    this.title.setTitle(`Author`);
  }

  loadAuthor(author_id) {
    this.busy_cat = true;
    this.wp.get_user(author_id)
      .subscribe((res) => {
        this.user = res;
        this.title.setTitle(`Author ${res['name']}`);
      }, (error) => {
        this.error = true;
        // console.log(error);
      });
  }

  loadAuthorPosts(author_id, value) {
    this.busy = true;
    this.wp.get_user_posts(author_id, 20)
      .subscribe((res) => {
        this.busy = false;
        this.posts = res;
      }, (error) => {
        this.error = true;
        // console.log(error);
      });
  }

  onUp() {
    this.direction = 'up';
  }

  doRefresh(event) {
    console.log(event);
    this.loadAuthor(this.author_id);
    this.loadAuthorPosts(this.author_id, 1);
    this.error = false;
  }


}
