import { Component, OnInit } from '@angular/core';
import { WpService } from '../services/wp.service';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.css']
})
export class CategoryListComponent implements OnInit {

  busy: boolean;
  categories: any;
  error: boolean;

  constructor(private wp: WpService) { }

  ngOnInit() {
    this.loadCategories();
  }

  loadCategories() {
    this.busy = true;
    this.wp.get_categories()
      .subscribe((res) => {
        // console.log(res);
        this.categories = res;
        this.busy = false;
      }, (error) => {
        this.error = true;
      });
  }

  doRefresh(event) {
    this.loadCategories();
    this.error = false;
  }
}
