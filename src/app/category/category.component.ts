import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { WpService } from '../services/wp.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  busy_cat: boolean;
  busy: boolean;
  category: any;
  posts: any;
  end: boolean;

  infinite_busy: boolean;

  throttle = 30;
  scrollDistance = 1;
  scrollUpDistance = 2;
  direction = '';
  page_count = 1;
  error: boolean;
  category_id: any;


  constructor(private router: Router,
    private wp: WpService,
    private route: ActivatedRoute,
    private title: Title) { }

  ngOnInit() {
    this.category_id = this.route.snapshot.params['category_id'];
    const category_slug = this.route.snapshot.params['category_slug'];

    this.loadCategory(this.category_id);
    this.loadCategoryPosts(this.category_id);
    this.title.setTitle('Category');
  }

  loadCategory(category_id) {
    this.busy_cat = true;
    this.wp.get_category(category_id)
      .subscribe((res) => {
        this.busy_cat = false;
        this.category = res;
        this.title.setTitle(res['name']);
      }, (error) => {
        this.error = true;
      });
  }

  loadCategoryPosts(category_id) {
    this.busy = true;
    this.wp.get_category_posts(category_id)
      .subscribe((res) => {
        this.busy = false;
        this.posts = res;
      });
  }

  onDown() {
    this.infinite_busy = true;
    this.direction = 'down';
    this.page_count = this.page_count + 1;


    this.wp.get_category_posts(20, this.page_count)
      .subscribe((res) => {
        if (!res['length']) {
          this.end = true;
          console.log('ended');
        } else {
          console.log('continuing');
          this.posts = this.posts.concat(res);
        }
      });
  }

  onUp() {
    this.direction = 'up';
  }

  doRefresh(event) {
    console.log(event);
    this.loadCategory(this.category_id);
    this.loadCategoryPosts(this.category_id);
    this.error = false;
  }

}
