# BlogNext

BlogNext is my next-gen version of [blog.khophi.co](blog.khophi.co), which you can see at [khophi.blog](www.khophi.blog). BlogNext uses Angular as the client-side framework, styled using UI Kit.

I built BlogNext with these principles in mind:

 - To meet all the principles of a Progressive Web Application
 - To load, if not instant, under 2 seconds, on every page.
 - To be clean, lean and simple (at least on the surface)

To achieve the above, [Angular Framework](angular.io) was the choice of front-end framework for the UI plumbing. The grooming and fashion part is handled by [UI Kit](https://getuikit.com).

The backend is [blog.khophi.co](blog.khophi.co) (a WordPress CMS), which exposes API endpoints of which the frontend consumes.

As you peruse the code, you should come across features making use of functionalities like

 - Angular Animations
 - The use of `@Input` and `@Ouput` for component interactions
 - Handling offline mode
 - Code reusability
 - HTTP Service
 - Infinite scrolling
 - Web Share API
 - Caching content for offline viewing

## Development

You can see the live demo currently running at [KhoPhi.Blog](khophi.blog).

You might be interested in playing with BlogNext on your local machine pulling data from your WordPress blog. Or perhaps even go live with it. 

For local development, see the steps below. For hosting online, check out the process in this blog post on KhoPhi's Dev Blog.

### Run Locally

 - Git clone this repository
 - `cd blognext && npm install`
 - In the `blognext/src/app/utils/constants.ts` file, replace the `DOMAIN_NAME` with your own. You could still maintain mine, if you don't have a WordPress running.
 - In the `blognext` directory, run `ng serve` to start the local server.
 - Open [localhost:4200](localhost:4200) in the browser to view.

## Contribution

Contribution are always welcome. Suggestions? Feedback? Send them in, preferrably via the Issues section. Raise an Issue, and I'll consider them.

Pull Requests too are welcome.

## License
**MIT** 

Rexford Nkansah 2018 <hello@khophi.co>